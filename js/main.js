
/**
 * Main AngularJS Web Application
 */
var app = angular.module('pendoApp', ['ngRoute', 'ngResource']);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "partials/home.html", controller: "mainCtrl"})
    // else 404
    .otherwise('404');
}]);
