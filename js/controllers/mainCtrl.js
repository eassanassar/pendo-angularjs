var app = angular.module('pendoApp');

app.controller('mainCtrl', function ($scope, Services, Helpers) {
  $scope.limitArray = new Array(10);
  $scope.loaded = true
  $scope.selectedCity = '';
  $scope.cities = [{
      label: 'Tel Aviv',
      coord: {
        lat: 32.0853,
        lon: 34.7818
      }
    }, {
      label: 'London',
      coord: {
        lat: 51.5074,
        lon: -0.1278
      }
    },
    {
      label: 'New York City',
      coord: {
        lat: 40.7128,
        lon: -74.0060
      }
  }];
  //fetch data and set it in the passes scope when changing cities
  $scope.changeCity = function(){
    $scope.loaded = false;
    var coord = $scope.selectedCity.coord;

    getPasses(coord, function(response){
      $scope.passes = response;
      $scope.loaded = true;
    });
  };

//fetching the passes then fetch the day/night for each pass
  var getPasses = function(coord, cb){
    Services.passes.get(coord ,null,  function(res){
      Helpers.setDayNight(res.response, coord, function(data){
        cb(data);
      });
    });
  };

});
