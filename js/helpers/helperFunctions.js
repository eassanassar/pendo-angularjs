var app = angular.module('pendoApp');

app.factory('Helpers', ['Services', function(Services) {

 return {
   //fetch all sunsrise-sunset times and return an array of object that have {rise, duration, Day/night object}
   setDayNight : function(arr, coord, cb, i){
     var newArray = [];
     var self = this;
     var cnt = 1;
     for(var i =0; i< arr.length; i++){
       self.getSunTime({lon: coord.lon, lat: coord.lat, date: self.getFormatDate(arr[i].risetime)}, arr[i], function(pass){
         cnt++;
         newArray.push(pass);
         if(cnt == arr.length){
           cb(newArray);
         }
       });
     }
   },

   //fetch the sunset-sunrise onject and return it added to the pass object
   getSunTime: function(reqObj, pass, cb){
     var self = this;
    Services.time.get(reqObj, function(data){
      pass.timeInd = self.getDayOrNight(data.results);
      cb(pass);
    })

   },
   //format date from timestamp to yyyy-mm-dd
   getFormatDate: function(timestamp){
     var date = new Date(timestamp);
     var newDate = date.getFullYear() +'-' + (date.getMonth()+1) + '-' + date.getDate();
     return newDate;
   },

   //if passTime is between sunseet and sunrise return 'NIGHT' otherwise return 'DAY'
   getDayOrNight : function(obj, passTime){
     var time = new Date(time).getTime();
     var sunrise = new Date(obj.sunrise).getTime();
     var sunset = new Date(obj.sunset).getTime();
     if(time < sunrise && time > sunset){
       return 'NIGHT';
     }else{
       return 'DAY';
     }

   }
 };

}]);
