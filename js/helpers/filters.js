var app = angular.module('pendoApp');

//convert seconds to HH:MM:SS
app.filter('hms', function() {
    return function(number) {
      if(isNaN(number) || number < 1) {
            return number;
       } else {
         var hours   = Math.floor(number / 3600);
         var minutes = Math.floor((number - (hours * 3600)) / 60);
         var seconds = number - (hours * 3600) - (minutes * 60);

         var output = (hours < 10 ? "0" + hours : hours);
           output += "-" + (minutes < 10 ? "0" + minutes : minutes);
           output += "-" + (seconds  < 10 ? "0" + seconds : seconds);

         return output;
       }
  }
});
//gets time stamp and conver it to YYYY (first 3 letters of the MONTH NAME) DD HH:MM:SS
app.filter('riseDateFormat', function() {
    return function(number) {
      if(isNaN(number) || number < 1) {
            return number;
       } else {
         var date= new Date(number);

         var year = date.getFullYear();
         var month = date.toString().slice(4,7);
         var day = date.getDate();
         var time =  date.toTimeString().slice(0,8);

         var output = year + ' ' + month + ' ' + day + ' ' + time;

         return output;
       }
  }
});
