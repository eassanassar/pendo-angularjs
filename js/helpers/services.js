var app = angular.module('pendoApp');

app.factory('Services', ['$resource', function($resource) {
 return {
   passes: $resource('http://api.open-notify.org/iss-pass.json?lat=:lat&lon=:lon', null, {
    get:{
      methode: 'GET',
      headers: {'Content-Type': 'text/html'}
    }
  }),
  time:  $resource('https://api.sunrise-sunset.org/json?lat=:lat&lng=:lon&date=:risetime&formatted=0', null, {
   get:{
     methode: 'GET',
     headers: {'Content-Type': 'text/html'}
   }
 }),
  }
}]);
